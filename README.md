# Goto Definition backtracker

This Sublime Text plugin enables to backtrack calls made with `Goto Definition`.  

## Usage

Once the `Goto Definition` command has been called, the `Goto Backwards` command will be available in the `Goto` menu and the Context menu (mouse right-click when clicking anywhere in a file).  

## Installation

This package is not in Package Control yet.  
Clone this repository in your local packages folder.  
If you are unsure as to where your local packages directory is, please consult the [docs](https://docs.sublimetext.io/guide/getting-started/basic-concepts.html#the-data-directory).  

Example (on MacOS):

```shell
cd ~/Library/Application\ Support/Sublime\ Text/Packages
git clone git@gitlab.com:patopest/sublime-plugin-goto-backwards.git Goto\ Backwards
``` 