import sublime
import sublime_plugin

stack = {}


class GotoBackwardsCommand(sublime_plugin.WindowCommand):

	def run(self, **args):
		key = str(self.window.window_id)
		last = stack[key].pop()
		word = last["word"]
		rowcol = last["rowcol"]
		# print("pop stack: ", last)

		filename = last["filename"] + ":{}:{}".format(rowcol[0], rowcol[1])

		flags = sublime.ENCODED_POSITION

		# Open view in Split view (aka Tab Multi-select)
		event = args.get("event", None)
		if (event):
			modifier_keys = event.get("modifier_keys", None)
			if (modifier_keys):
				if (modifier_keys.get("primary", False) or modifier_keys.get("super", False)): #Is Cmd key pressed?
					flags |= sublime.ADD_TO_SELECTION

		view = self.window.open_file(filename, flags=flags)
		
		# Show selection of the word
		sel = view.sel()
		sel.clear()
		sel.add(sublime.Region(word.begin(), word.end()))

		return None

	def is_enabled(self):
		key = str(self.window.window_id)
		if (len(stack.get(key, [])) == 0):
			return False
		else:
			return True

	def is_visible(self):
		key = str(self.window.window_id)
		if (len(stack.get(key, [])) == 0):
			return False
		else:
			return True

	def want_event(self):
		return True



class GotoBackwardsEmptyStack(sublime_plugin.WindowCommand):

	def run(self, **args):
		key = str(self.window.window_id)
		# print("Deleting stack for window {}".format(key))
		del stack[key]

		return None

	def is_enabled(self):
		key = str(self.window.window_id)
		if (len(stack.get(key, [])) == 0):
			return False
		else:
			return True

	def is_visible(self):
		return True



class GotoDefinition(sublime_plugin.EventListener):

	def on_window_command(self, window, command_name, args):

		if (command_name == "goto_definition"):
			sheet = window.active_sheet()
			view = window.active_view()

			data = {}

			# args are only passed if goto_definition was triggerd by event (i.e.: click on screen)
			if (args):
				data = args.get("event")
				region = view.window_to_text((data["x"], data["y"]))

			# Otherwise (i.e: key shortcut) find the cursor region
			else:
				region = view.sel()[0]

			word = view.word(region)
			rowcol = view.rowcol(word.begin())

			data.update({
				"sheet": sheet,
				"filename": sheet.file_name(),
				"region": region,
				"word": word,
				"rowcol": rowcol
			})

			key = str(window.window_id)
			if (key in stack):
				stack[key].append(data)
			else:
				stack[key] = [data]
			# print("add to stack: ", data)

		return None


